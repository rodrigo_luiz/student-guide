# Student guide

<p>
Este repositório é destinado aos meus estudos e projetos de design de interface de usuário (UI) e experiência de usuário (UX), bem como programação web.
</p>

<p>  
Aqui você encontrará projetos para aprimorar habilidades em criação de interfaces amigáveis e intuitivas para o usuário, bem como protótipos de experiências de usuário e pesquisas de campo. Além disso, você também encontrará aplicativos web desenvolvido utilizando linguagens como HTML, CSS e JavaScript.


- Sinta-se à vontade para dar uma olhada e me fornecer feedback sobre os trabalhos!
